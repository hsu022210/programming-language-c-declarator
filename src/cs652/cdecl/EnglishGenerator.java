package cs652.cdecl;

public class EnglishGenerator extends CDeclBaseVisitor<String> {
	// YOU MUST FILL THIS IN

    @Override
    public String visitArray(CDeclParser.ArrayContext ctx) {
        return visit(ctx.declarator()) + " array of";
    }

    @Override
    public String visitFunc(CDeclParser.FuncContext ctx) {
        return visit(ctx.declarator()) + " function returning";
    }

    @Override
    public String visitPointer(CDeclParser.PointerContext ctx) {
        return visit(ctx.declarator()) + " pointer to";
    }

    @Override
    public String visitGrouping(CDeclParser.GroupingContext ctx) {
        return visit(ctx.declarator());
    }

    @Override
    public String visitVar(CDeclParser.VarContext ctx) {
        return (ctx.ID().getText() + " is a");
    }

    @Override
    public String visitDeclaration(CDeclParser.DeclarationContext ctx) {
        String typename = ctx.typename().getText();
        if (typename.equals("void")){
            typename = "nothing";
        }
        return visit(ctx.declarator()) + " " + typename;
    }
}
