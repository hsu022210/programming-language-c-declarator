package cs652.cdecl;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Tool {

	public static void main(String[] args) {
		// YOU MUST FILL THIS IN
		String result = translate("int i;");
		System.out.println(result);
	}

	public static String translate(String cdeclText) {
		// YOU MUST FILL THIS IN
		ANTLRInputStream input = new ANTLRInputStream(cdeclText);
		CDeclLexer lexer = new CDeclLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		CDeclParser parser = new CDeclParser(tokens);
		ParseTree tree = parser.declaration();

		EnglishGenerator visitor = new EnglishGenerator();
		return visitor.visit(tree);
	}
}

